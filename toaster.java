 public class toaster {
	 
	 public double size;
	 public int toasts;
	 public String colour;
	 
	 public void printSize() {
		 System.out.println("this is the size of your toaster: " + size);
	 }
	 
	 public void cooksBread() {
		 System.out.println("your toaster can make " + toasts + " toasts at once.");
	 }
 }
	 